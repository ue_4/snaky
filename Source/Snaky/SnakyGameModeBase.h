// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKY_API ASnakyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
