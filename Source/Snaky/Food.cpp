// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "Math/UnrealMathUtility.h"

int AFood::FoodAmount = 2;

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//SpawnFood();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto SnakeEating = Cast<ASnake>(Interactor);
		if (IsValid(SnakeEating))
		{
			SnakeEating->AddSnakeElement();
			SnakeEating->SpeedUp(AddSpeed);
			FoodAmount -= 1;
			if (FoodAmount < 2)
			{
				SpawnFood();
			}
			this->Destroy();
		}
	}
}

void AFood::SpawnFood()
{
	FoodAmount += 1;

	FVector RandomLocation(FMath::RandRange(-400, 400), FMath::RandRange(-400, 400), 0); //to make a check for head overlap
	FoodSpawnTransform = FTransform(RandomLocation);
	GetWorld()->SpawnActor<AFood>(FoodSpawnClass, FoodSpawnTransform);
}


